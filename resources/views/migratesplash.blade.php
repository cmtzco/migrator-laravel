<html>
	<head>
		<title>Laravel</title>
		
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: top;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 60px;
				margin-top: 15px;
			}

			.form-details {
				width: 45%;
				margin: 0 auto;
				font-size: 20px;
				border: 1px #888 solid;
				border-radius:2px;
				box-shadow: 2px 2px 5px #888888;
				height:inherit;
			}
			label {
				margin-top: 15px;
				color: #4D4D4D;
				font-weight: bold;
				padding-right: 10px;
				padding-left: 25px;
 				display: block;
    				float: left;
			}
			input {
				font-size: 16px;
 				display: inline-block;
				width: 90%;
			}
			select {
				font-size: 16px;
                                display: inline-block;
                                width: 90%;
			}
			input[type='submit'] {
				margin-top:25px;
				width: 35%;
				height: 35px; 
			}
				
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title">Migrator v 0.1</div>
				<div class="form-details">
					{!! Form::open(array('url' => 'upload/csv')) !!}
					{!! Form::label('storeurl', "Store URL") !!}
						{!! Form::text('storeurl') !!}
					{!! Form::label('storetoken', "API Token") !!}
                                                {!! Form::password('storetoken') !!}
					{!! Form::label('importtype', "Type of Import") !!}
                                                {!! Form::select('importtype', array('products' => 'Products', 'categories' => 'Categories', 'options' => 'Options', 'customers' => 'Customers', 'rules' => 'Rules', 'images' => 'Images'), 'products') !!}
					{!! Form::label('csv', "Upload a CSV") !!}
                                                {!! Form::file('csv') !!}
					{!! Form::submit('Begin Import') !!}
					
					
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</body>
</html>
